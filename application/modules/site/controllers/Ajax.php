<?php
class Ajax extends MY_Controller {

  public function __construct()
  {
      parent::__construct();
      setlocale (LC_TIME, 'id_ID');
  }

  function now() {
    //echo date('D, d M Y H:i:s');

    $dt = json_encode(array(
      'Day'=>date('D'),
      'Date'=>date('d'),
      'Month'=>date('M'),
      'Year'=>date('Y'),
      'Hour'=>date('H'),
      'Minute'=>date('i'),
      'Second'=>date('s')
    ));
    echo $dt;
  }

  public function get_weather() {
    $lat = 3.326288;
    $lon = 99.156685;

    $wilayah = json_decode(file_get_contents("https://ibnux.github.io/BMKG-importer/cuaca/wilayah.json"),true);
    $jml = count($wilayah);

    //  hitung jarak
    for($n=0;$n<$jml;$n++){
        $wilayah[$n]['jarak'] = $this->distance($lat, $lon, $wilayah[$n]['lat'], $wilayah[$n]['lon'], 'K');
    }

    //urutkan
    usort($wilayah, array($this,'urutkanJarak'));

    //ambil 5 besar aja
    //echo "<pre>";
    //echo "\n<h2>Urutkan dari yang terdekat<br>$lat,$lon</h2>\n";
    //echo "\n";
    for($n=0;$n<5;$n++){
        //print_r($wilayah[$n]);
        //echo "\n";
    }
    //echo "\n<h2>";
    //echo $wilayah[0]['propinsi'].",".$wilayah[0]['kota'].",".$wilayah[0]['kecamatan']."\n";
    //echo number_format($wilayah[0]['jarak'],2,",",".")." km</h2>\n";
    //echo "\n";

    //ambil cuaca kota terdekat
    $json = json_decode(file_get_contents("https://ibnux.github.io/BMKG-importer/cuaca/".$wilayah[0]['id'].".json"),true);
    $resp = array();
    $n = 0;
    //echo '<table border="1"><tr>';
    foreach($json as $cuaca){
        $timeCuaca = strtotime($cuaca['jamCuaca']);
        //yang lewat ngga perlu ditampilkan
        if(date('Y-m-d', $timeCuaca) == date('Y-m-d') && date('H:i', $timeCuaca) > "00:00"){
            // '<td>';
            //echo '<img src="https://ibnux.github.io/BMKG-importer/icon/'.$cuaca['kodeCuaca'].'.png" class="image">';
            //echo '<p>'.$cuaca['cuaca'].'</p>';
            //echo "</td>\n";
            $resp[] = $cuaca;
        }
    }
    //echo '</tr><table>';
    //echo "\n";

    //echo json_encode($resp);
    if(!empty($resp)) {
      $html = "";
      foreach($resp as $r) {
        $nmCuaca = $r['cuaca'];
        $kdCuaca = $r['kodeCuaca'];
        $nmWaktu = date('H:i', strtotime($r['jamCuaca']));
        $numHumidity = $r['humidity'];
        $numTempC = $r['tempC'];
        $numTempF = $r['tempF'];
        $c = @"
        <div class=\"card bg-light\">
          <div class=\"card-body\">
            <div class=\"row\">
              <div class=\"col-8 text-sm\">
                <h2 class=\"lead\"><b>$nmCuaca</b></h2>
                <div class=\"row\">
                  <div class=\"col-sm-6 text-right\">Waktu :</div>
                  <div class=\"col-sm-6 font-weight-bold\">$nmWaktu</div>
                </div>
                <div class=\"row\">
                  <div class=\"col-sm-6 text-right\">Temperatur :</div>
                  <div class=\"col-sm-6\">$numTempC C, $numTempF F</div>
                </div>
                <div class=\"row\">
                  <div class=\"col-sm-6 text-right\">Kelembapan :</div>
                  <div class=\"col-sm-6\">$numHumidity</div>
                </div>
              </div>
              <div class=\"col-4 text-center\">
                <img src=\"https://ibnux.github.io/BMKG-importer/icon/$kdCuaca.png\" alt=\"\" class=\"img-fluid\">
              </div>
            </div>
          </div>
        </div>
        ";
        $html .= $c;
      }
      echo $html;
    } else {
      echo '<p>Data tidak tersedia.</p>';
    }
  }

  function urutkanJarak($a, $b) {
    return $a['jarak'] - $b['jarak'];
  }

  // https://www.geodatasource.com/developers/php
  function distance($lat1, $lon1, $lat2, $lon2, $unit) {
      if (($lat1 == $lat2) && ($lon1 == $lon2)) {
          return 0;
      }
      else {
          $theta = $lon1 - $lon2;
          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
          $unit = strtoupper($unit);
          if ($unit == "K") {
              return ($miles * 1.609344);
          } else if ($unit == "N") {
              return ($miles * 0.8684);
          } else {
              return $miles;
          }
      }
  }
}
