<?php
$ruser = GetLoggedUser();
?>
<div class="row">
<?php
if($data[COL_STATUS] == 'PEMANTAUAN_SELESAI') {
  ?>
  <div class="col-sm-12">
    <p>Status sudah <strong>SELESAI PEMANTAUAN</strong>.</p>
  </div>
  <?php
}
else if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEMEDIS && $data[COL_STATUS] == 'PEMANTAUAN') {
  ?>
  <div class="col-sm-12">
    <p>Status masih dalam <strong>PEMANTAUAN</strong>.</p>
  </div>
  <?php
} else if($data[COL_STATUS] == 'PEMANTAUAN_SELESAI') {

} else {
  ?>
  <?=form_open(current_url(),array('role'=>'form','id'=>'form-forward','style'=>'width: 100%'))?>
  <div class="col-sm-12">
    <table class="table table-bordered text-sm">
      <tbody>
        <tr>
          <td class="nowrap text-right va-middle" width="100px">Kategori</td><td class="va-middle" width="10px">:</td>
          <td><?=$data[COL_JENIS]?></td>
        </tr>
        <tr>
          <td class="nowrap text-right va-middle" width="100px">Disposisi</td><td class="va-middle" width="10px">:</td>
          <td>
            <select name="<?=COL_STATUS?>" class="form-control" style="width: 100%">
              <?php
              if(($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEGUGUS) && ($data[COL_STATUS]=='VERIFIKASI' || $data[COL_STATUS]=='PEMANTAUAN')) {
                ?>
                <option value="PEMANTAUAN">PEMANTAUAN</option>
                <?php
              }
              ?>
              <option value="PEMANTAUAN_SELESAI">PEMANTAUAN SELESAI</option>
            </select>
          </td>
        </tr>
        <?php
        if($data[COL_STATUS]=='VERIFIKASI' && ($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEGUGUS)) {
          ?>
          <tr>
            <td class="nowrap text-right va-middle" width="100px">Rujukan</td><td class="va-middle" width="10px">:</td>
            <td>
              <select name="<?=COL_ID_PUSKESMAS?>" class="form-control" style="width: 100%">
                <?=GetCombobox("SELECT * FROM covid19_puskesmas order by Nm_Puskesmas", COL_KD_PUSKESMAS, COL_NM_PUSKESMAS, (!empty($data[COL_ID_PUSKESMAS]) ? $data[COL_ID_PUSKESMAS] : null))?>
              </select>
            </td>
          </tr>
          <?php
        } else if($data[COL_STATUS]=='PEMANTAUAN' && ($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEMEDIS)) {
          ?>
          <tr>
            <td class="nowrap text-right va-middle" width="100px">Gejala</td><td class="va-middle" width="10px">:</td>
            <td>
              <?php
              $arrKesehatan = explode(',', $data[COL_KESEHATAN]);
              ?>
              <select name="<?=COL_KESEHATAN?>[]" class="form-control" style="width: 100%" multiple>
                <option value="DEMAM" <?=in_array('DEMAM', $arrKesehatan)?'selected':''?>>DEMAM</option>
                <option value="PILEK" <?=in_array('PILEK', $arrKesehatan)?'selected':''?>>PILEK</option>
                <option value="BATUK" <?=in_array('BATUK', $arrKesehatan)?'selected':''?>>BATUK</option>
                <option value="SAKIT TENGGOROKAN" <?=in_array('SAKIT TENGGOROKAN', $arrKesehatan)?'selected':''?>>SAKIT TENGGOROKAN</option>
                <option value="SESAK NAFAS" <?=in_array('SESAK NAFAS', $arrKesehatan)?'selected':''?>>SESAK NAFAS</option>
              </select>
            </td>
          </tr>
          <tr>
            <td class="nowrap text-right va-middle" width="100px">Suhu Badan</td><td class="va-middle" width="10px">:</td>
            <td class="p-0">
              <table class="table m-0 border-0">
                <tbody>
                  <tr>
                    <td>
                      <input type="text" class="form-control" placeholder="Celcius" style="text-align: right" name="<?=COL_SUHU_BADAN?>" value="<?=$data[COL_SUHU_BADAN]?>" />
                    </td>
                    <td class="va-middle">
                      <div class="form-group m-0">
                        <label class="m-0">Diagnosa: </label>&nbsp;&nbsp;
                        <div class="form-check d-inline">
                          <input class="form-check-input" type="radio" id="checkboxSehat" name="<?=COL_DIAGNOSA?>" value="SEHAT" <?=$data[COL_DIAGNOSA]=='SEHAT'?'checked':(empty($data[COL_DIAGNOSA])?'checked':'')?>>
                          <label class="form-check-label" for="checkboxSehat">SEHAT</label>
                        </div>&nbsp;
                        <div class="form-check d-inline">
                          <input class="form-check-input" type="radio" id="checkboxSakit" name="<?=COL_DIAGNOSA?>" value="SAKIT" <?=$data[COL_DIAGNOSA]=='SAKIT'?'checked':''?>>
                          <label class="form-check-label" for="checkboxSakit">SAKIT</label>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td class="nowrap text-right va-middle" width="100px">Catatan Medis</td><td class="va-middle" width="10px">:</td>
            <td>
              <textarea name="<?=COL_CATATAN_MEDIS?>" class="form-control" rows="3"><?=$data[COL_CATATAN_MEDIS]?></textarea>
            </td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
  <div class="col-md-12 text-right">
    <button type="submit" class="btn btn-outline-danger"><i class="far fa-save"></i>&nbsp;SUBMIT</button>
  </div>
  <?=form_close()?>
  <?php
}
?>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#form-forward').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            /*if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }*/
            $('#datalist').DataTable().ajax.reload();
            $('#form-forward').closest('.modal').modal('hide');
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
