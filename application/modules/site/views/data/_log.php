<div class="col-sm-12">
  <?php
  if($data[COL_STATUS]=='SELESAI') {
    echo 'Log tidak dapat ditambah lagi.';
  } else {
    ?>
    <?=form_open(current_url(),array('role'=>'form','id'=>'form-log','style'=>'width: 100%'))?>
    <table class="table table-bordered text-sm">
      <tbody>
        <tr>
          <td class="nowrap text-right" width="100px">Kategori</td><td width="10px">:</td>
          <td><?=$data[COL_JENIS]?></td>
        </tr>
        <tr>
          <td class="nowrap text-right" width="100px">Disposisi</td><td width="10px">:</td>
          <td><?=$data[COL_STATUS]?></td>
        </tr>
        <tr>
          <td class="nowrap text-right" width="100px">Tanggal Pemantauan</td><td width="10px">:</td>
          <td>
            <input type="text" class="form-control datepicker" name="<?=COL_TANGGAL_LOG?>" style="width: 10vw" />
          </td>
        </tr>
        <tr>
          <td class="nowrap text-right" width="100px">Catatan</td><td width="10px">:</td>
          <td>
            <textarea class="form-control" rows="3" name="<?=COL_CATATAN?>"></textarea>
          </td>
        </tr>
        <tr>
          <td colspan="3" class="text-right">
            <button type="submit" class="btn btn-sm btn-outline-danger"><i class="far fa-save"></i>&nbsp;SUBMIT</button>
          </td>
        </tr>
      </tbody>
    </table>
    <?=form_close()?>
    <?php
  }
  ?>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#form-log').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            $('#datalist').DataTable().ajax.reload();
            $('#form-log').closest('.modal').modal('hide');
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
