<style>
.todo-list>li:hover {
    background-color: #ccc;
}
.carousel-container {
  min-height: 300px;
}

#section-data .card-inner {
  transition: transform .2s;
}
#section-data .card-inner:hover {
  transform: scale(1.1);
  z-index: 1;
}
.timeline::before {
  display: none;
}
#section-data .content-header-bg::after {
  background: url(<?=MY_IMAGEURL.'footer-map-bg.png'?>) no-repeat scroll left top / 100% auto;
  content: "";
  height: 100%;
  left: 0;
  opacity: 0.1;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
}
#tbl-data-kec th {
  vertical-align: middle;
  text-align: center;
}
</style>
<?php
$carouselDir =  scandir(MY_IMAGEPATH.'slide/');
$carouselArr = array();
foreach (scandir(MY_IMAGEPATH.'slide/') as $file) {
  if(strpos(mime_content_type(MY_IMAGEPATH.'slide/'.$file), 'image') !== false) {;
    $carouselArr[MY_IMAGEPATH.'slide/'.$file] = filemtime(MY_IMAGEPATH.'slide/'. $file); //$carouselArr[] = MY_IMAGEURL.'slide/'.$file;
  }
}
arsort($carouselArr);
$carouselArr = array_keys($carouselArr);
?>
<div class="content-wrapper mt-0">
  <div class="hero-container">
    <span class="hero-wave"></span>
    <div class="hero-content">
      <div class="row">
        <div class="col-sm-4">
          <div class="hero-item text-left">
            <h4 class="font-weight-bold" style="color: #0000a4"><small class="font-weight-bold text-white">PORTAL WEBSITE RESMI</small><br /><?=$this->setting_org_name?></h4>
          </div>
        </div>
        <div class="col-sm-8">
          <!-- SLIDER -->
          <?php
          if(!empty($carouselArr)) {
            ?>
            <div id="carouselMain" class="carousel slide carousel-fade" data-ride="carousel" style="border: 4px solid #0000a4; box-shadow: 0 0 5px #00000020, 0 3px 5px rgba(0,0,0,.2);">
              <div class="carousel-inner">
                <?php
                for($i=0; $i<count($carouselArr); $i++) {
                  ?>
                  <div class="carousel-item <?=$i==0?'active':''?>">
                    <!--<img class="d-block" src="<?=$carouselArr[$i]?>" style="height: 200px">-->
                    <div class="d-block w-100" style="background: url('<?=$carouselArr[$i]?>'); background-size: cover; background-repeat: no-repeat; height: 270px">
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
              <a class="carousel-control-prev" href="#carouselMain" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselMain" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <ol class="carousel-indicators" style="transform: translateY(50px);">
                <?php
                for($i=0; $i<count($carouselArr); $i++) {
                  ?>
                  <li data-target="#carouselMain" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                  <?php
                }
                ?>
              </ol>
            </div>

            <?php
          }
          ?>
          <!-- SLIDER -->
        </div>
      </div>
    </div>
  </div>
  <div class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="card card-outline card-orange">
            <div class="card-header">
              <h5 class="card-title m-0">Berita Terkini</h5>
            </div>
            <div class="card-body">
              <?php
              if(!empty($berita)) {
                foreach($berita as $b) {
                  $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
                  $img = $this->db->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
                  ?>
                  <div class="d-block pb-3 mb-3" style="border-bottom: 1px solid #dedede">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="img" style="
                        height: 150px;
                        width: 100%;
                        background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_FILENAME]:MY_IMAGEURL.'no-image.png'?>');
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-position: center;
                        border: 4px solid #0000a4;
                        ">
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>" style="color: #0000a4"><h5 style="color: #0000a4"><?=$b[COL_POSTTITLE]?></h5></a>
                        <p class="mb-0">
                          <small class="text-muted"><?=date('d-m-Y H:i', strtotime($b[COL_CREATEDON]))?></small>
                          <small class="text-muted float-right">dilihat <strong><?=$b[COL_TOTALVIEW]?></strong> kali</small>
                        </p>
                        <p class="mb-2">
                          <?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?>
                        </p>
                      </div>
                    </div>
                  </div>
                  <?php
                }
              } else {
                echo 'Belum ada data.';
              }
              ?>
              <div class="row">
                <div class="col-sm-12">
                  <a href="<?=site_url('site/home/post/1')?>" class="btn btn-block bg-black" style="background-color: #f0843b">LIHAT SEMUA</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="card card-outline card-orange">
            <div class="card-header">
              <h5 class="card-title m-0">Prakiraan Cuaca <strong class="text-sm">(<?=date('d-m-Y')?>)</strong></h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div id="forecast" class="col-sm-12">
                  <p>Memuat...</p>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-body p-0">
              <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
              <div id="gpr-kominfo-widget-container"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
var lat = 3.326288;
var lon = 99.156685;
function getWilayah() {
    $.getJSON('https://ibnux.github.io/BMKG-importer/cuaca/wilayah.json', function (data) {
        var items = [];
        var jml = data.length;

        //hitung jarak
        for (n = 0; n < jml; n++) {
            data[n].jarak = distance(lat, lon, data[n].lat, data[n].lon, 'K');
        }

        //urutkan berdasarkan jarak
        data.sort(urutkanJarak);

        //setelah dapat jarak,  ambil 5 terdekat
        for (n = 0; n < jml; n++) {
            items.push('<li class="list-group-item d-flex justify-content-between align-items-center">' + data[n].propinsi +
                ', ' + data[n].kota + ', ' + data[n].kecamatan + '<span class="badge badge-primary badge-pill">' + data[n].jarak.toFixed(2) + ' km</span></li>');
            if (n > 4) break
        };
        $('#judulTerdekat').html("Jarak terdekat dari " + lat + "," + lon);
        $('#wilayahTerdekat').html(items.join(""));
        $('#judulCuaca').html(data[0].propinsi +
                ', ' + data[0].kota + ', ' + data[0].kecamatan + ' ' + data[0].jarak.toFixed(2)+" km");
        getCuaca(data[0].id);
    });
}

function getCuaca(idWilayah) {
    $.getJSON('https://ibnux.github.io/BMKG-importer/cuaca/'+idWilayah+'.json', function (data) {
        var items = [];
        data = jQuery.grep(data, function(n) {
          return (moment(n.jamCuaca).format("YYYY-MM-DD") == '<?=date('Y-m-d')?>' && moment(n.jamCuaca).format("HH") != '00');
        });
        var jml = data.length;

        //setelah dapat jarak,  ambil 5 terdekat
        for (n = 0; n < jml; n++) {
          items.push('<div class=\"card bg-light\">'+
          '<div class=\"card-body\">'+
          '<div class=\"row\">'+
          '<div class=\"col-8 text-sm\">'+
                '<h2 class=\"lead\"><b>'+data[n].cuaca+'</b></h2>'+
                  '<div class=\"row\">'+
                  '<div class=\"col-sm-6 text-right\">Waktu :</div>'+
                    '<div class=\"col-sm-6 font-weight-bold\">'+moment(data[n].jamCuaca).format("HH:mm")+'</div>'+
                    '</div>'+
                  '<div class=\"row\">'+
                  '<div class=\"col-sm-6 text-right\">Temperatur :</div>'+
                    '<div class=\"col-sm-6\">'+data[n].tempC+' C, '+data[n].tempF+' F</div>'+
                    '</div>'+
                  '<div class=\"row\">'+
                  '<div class=\"col-sm-6 text-right\">Kelembapan :</div>'+
                    '<div class=\"col-sm-6\">'+data[n].humidity+'</div>'+
                    '</div>'+
                  '</div>'+
                '<div class=\"col-4 text-center\">'+
                '<img src=\"https://ibnux.github.io/BMKG-importer/icon/'+data[n].kodeCuaca+'.png\" alt=\"\" class=\"img-fluid\">'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>');
            if (n > 4) break
        };
        $('#forecast').html(items.join(""));
    });
}

// https://www.htmlgoodies.com/beyond/javascript/calculate-the-distance-between-two-points-in-your-web-apps.html
function distance(lat1, lon1, lat2, lon2) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    return Math.round((dist * 1.609344) * 1000) / 1000;
}

function urutkanJarak(a, b) {
    if (a['jarak'] === b['jarak']) {
        return 0;
    }
    else {
        return (a['jarak'] < b['jarak']) ? -1 : 1;
    }
}
getWilayah();

$(document).ready(function() {
  /*$('#forecast').load('<?=site_url('site/ajax/get-weather')?>', function(){

  });*/
});
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox({
    alwaysShowClose: true
  });
});
(function ($) {
  var navbar = $('.navbar');
  var lastScrollTop = 0;

  $(window).scroll(function () {
      var st = $(this).scrollTop();
      // Scroll down
      if (st > lastScrollTop) {
          //navbar.fadeOut();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-light bg-white');
      }
      // Scroll up but still lower than 200 (change that to whatever suits your need)
      else if(st < lastScrollTop && st > 200) {
          //navbar.fadeIn();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-light bg-white');
      }
      // Reached top
      else {
          navbar.removeClass('navbar-light bg-white').addClass('navbar-dark bg-transparent');
      }
      lastScrollTop = st;
  }).trigger('scroll');
})(jQuery);
</script>
