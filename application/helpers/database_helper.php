<?php
define('TBL__POSTCATEGORIES','_postcategories');
define('TBL__POSTIMAGES','_postimages');
define('TBL__POSTS','_posts');
define('TBL__ROLES','_roles');
define('TBL__SETTINGS','_settings');
define('TBL__USERINFORMATION','_userinformation');
define('TBL__USERS','_users');
define('TBL_TLAPORAN','tlaporan');

define('COL_POSTCATEGORYID','PostCategoryID');
define('COL_POSTCATEGORYNAME','PostCategoryName');
define('COL_POSTCATEGORYLABEL','PostCategoryLabel');
define('COL_POSTIMAGEID','PostImageID');
define('COL_POSTID','PostID');
define('COL_FILENAME','FileName');
define('COL_DESCRIPTION','Description');
define('COL_POSTDATE','PostDate');
define('COL_POSTTITLE','PostTitle');
define('COL_POSTSLUG','PostSlug');
define('COL_POSTCONTENT','PostContent');
define('COL_POSTEXPIREDDATE','PostExpiredDate');
define('COL_TOTALVIEW','TotalView');
define('COL_LASTVIEWDATE','LastViewDate');
define('COL_ISSUSPEND','IsSuspend');
define('COL_CREATEDBY','CreatedBy');
define('COL_CREATEDON','CreatedOn');
define('COL_UPDATEDBY','UpdatedBy');
define('COL_UPDATEDON','UpdatedOn');
define('COL_ROLEID','RoleID');
define('COL_ROLENAME','RoleName');
define('COL_SETTINGID','SettingID');
define('COL_SETTINGLABEL','SettingLabel');
define('COL_SETTINGNAME','SettingName');
define('COL_SETTINGVALUE','SettingValue');
define('COL_USERNAME','UserName');
define('COL_EMAIL','Email');
define('COL_NAME','Name');
define('COL_JABATAN','Jabatan');
define('COL_NO_IDENTITAS','No_Identitas');
define('COL_NO_UNITKERJA','No_UnitKerja');
define('COL_NO_HP','No_HP');
define('COL_IMAGEFILENAME','ImageFilename');
define('COL_PASSWORD','Password');
define('COL_LASTLOGIN','LastLogin');
define('COL_LASTLOGINIP','LastLoginIP');
define('COL_UNIQ','Uniq');
define('COL_NMPELAPOR','NmPelapor');
define('COL_NMNIK','NmNIK');
define('COL_NMNOMORHP','NmNomorHP');
define('COL_NMALAMAT','NmAlamat');
define('COL_NMLAPORAN','NmLaporan');
